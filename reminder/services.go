package reminder

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/l143/golang/r3mindr/storage"
)

const entryFormat = "02 Jan 2006 at 15:04"

func CreateReminder(title string, description string, date string, silent bool, force bool) (Reminder, error) {
	database, _ := sql.Open("sqlite3", storage.GetDBPath())

	rem := EmptyReminder()
	tD, timedate_err := time.Parse(entryFormat, date)
	if timedate_err != nil {
		return rem, timedate_err
	}

	time_date := time.Date(tD.Year(), tD.Month(), tD.Day(), tD.Hour(), tD.Minute(), 0, 0, time.Local)

	if !force {
		now := time.Now()
		if time_date.Before(now) {
			errMesg := fmt.Sprintf("%s is before %s", time_date.Format(entryFormat), now.Format(entryFormat))
			return rem, errors.New(errMesg)
		}
	}

	remDto := CreateReminderDto{
		title:       title,
		description: description,
		date:        time_date,
		shouldAlert: !silent,
	}

	repo := NewReminderRepo(database)
	return repo.save(remDto)

}

func GetReminders(filter_by FilterBy) ([]Reminder, error) {
	database, _ := sql.Open("sqlite3", storage.GetDBPath())
	repo := NewReminderRepo(database)
	return repo.list(filter_by)
}

func GetReminder(id int64) (Reminder, error) {
	database, _ := sql.Open("sqlite3", storage.GetDBPath())
	repo := NewReminderRepo(database)

	return repo.get(id)
}

func UpdateReminder(rem Reminder, partial UpdateReminderDto) (Reminder, error) {
	database, _ := sql.Open("sqlite3", storage.GetDBPath())
	repo := NewReminderRepo(database)
	return repo.update(rem, partial)
}
