package cli

import (
	"fmt"

	"github.com/spf13/cobra"
)

const format_flag = "format"

func CommandExport() *cobra.Command {
	exportCmd := &cobra.Command{
		Use:   "export",
		Short: "Export all reminders",
		Run:   runExportFn(),
	}

	exportCmd.Flags().StringP(format_flag, "f", "csv", "Format of the exported file")
	return exportCmd
}

func runExportFn() CobraFn {
	return func(cmd *cobra.Command, args []string) {
		format, _ := cmd.Flags().GetString(format_flag)
		switch {
		case format == "csv":
			fmt.Println("Exporting in csv")
		default:
			fmt.Println("Inavlid format option")
		}
	}
}
