#!/bin/bash

./build/build.sh

# Move files to deb package
mv ./r3-cli ./build/deb/usr/local/bin
mv ./r3-server ./build/deb/usr/local/bin

# Copy the assets
cp ./assets/icon1x.png ./build/deb/usr/local/bin/r3

echo "[2] Packaging ..."
dpkg -b ./build/deb ./build/r3.deb