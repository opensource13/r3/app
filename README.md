<img width="200" src="assets/icon1x.png">

# R3-reminder

Cli app based in go & cobra to create reminders and store them in sqlite db.

The project const in a cli to manage the reminders and a server that will be in charge of pushing the notifications.


Build
--
```bash
$ go build ./cmd/server/r3-server.go # Start the server 
```

```bash
$ go build ./cmd/cli/r3-cli.go # Manage reminder
```

Usage
--

```bash
$ ./server # Start the server 
```
![server start](./assets/Server_Screenshot.png)

```bash
$ ./cli # Manage reminder
```

![server start](./assets/Cli_Screenshot.png)


```
r3-cli

Usage:
  r3 [command]

Available Commands:
  add         Add a reminder
  completion  generate the autocompletion script for the specified shell
  export      Export all reminders
  help        Help about any command
  init        init application [createdb]
  list        List all reminders
  notify      notify

Flags:
  -h, --help   help for r3

Use "r3 [command] --help" for more information about a command.
```

## Todo
- [ ] Edit reminder
- [ ] Export reminders
- [ ] Add Server daemons for operative systems