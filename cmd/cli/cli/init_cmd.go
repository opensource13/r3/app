package cli

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/l143/golang/r3mindr/storage"
)

func CommandInit() *cobra.Command {
	initCmd := &cobra.Command{
		Use:   "init",
		Short: "init application [createdb]",
		Run:   runInitFn(),
	}

	return initCmd
}

func runInitFn() CobraFn {
	return func(cmd *cobra.Command, args []string) {
		storage.InitDB()

		fmt.Println("DB initialized")
	}
}
