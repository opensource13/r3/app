package reminder

import (
	"database/sql"
	"errors"
	"fmt"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

type ReminderRepo struct {
	repo *sql.DB
}

func NewReminderRepo(sql *sql.DB) *ReminderRepo {
	return &ReminderRepo{repo: sql}
}

func (r ReminderRepo) save(rem CreateReminderDto) (Reminder, error) {
	statement, _ := r.repo.Prepare("INSERT INTO reminders (title, description, date, should_alert) VALUES (?, ?, ?, ?)")
	result, err := statement.Exec(rem.title, rem.description, rem.date.Format(time.RFC3339), rem.shouldAlert)
	if err != nil {
		errMsg := fmt.Sprintf("Query Error %s", err)
		return Reminder{id: 0, shouldAlert: false, notified: false, date: time.Now()}, errors.New(errMsg)
	}

	id, _ := result.LastInsertId()

	return Reminder{
			id:          id,
			title:       rem.title,
			description: rem.description,
			notified:    false,
			shouldAlert: rem.shouldAlert,
			date:        rem.date},
		nil
}

func (r ReminderRepo) get(search_id int64) (Reminder, error) {
	rem := EmptyReminder()
	query := fmt.Sprintf("SELECT id, title, description, date, notified, should_alert FROM reminders WHERE id = %d", search_id)
	rows, err := r.repo.Query(query)
	if err != nil {
		return rem, err
	}

	reminders, parseErr := parseRowsToReminders(rows)
	if parseErr != nil {
		return rem, parseErr
	}

	rem = reminders[0]

	if rem.id == 0 {
		return rem, errors.New("Reminder not found")
	}
	return rem, nil
}

func (r ReminderRepo) update(originalRem Reminder, partial UpdateReminderDto) (Reminder, error) {
	updatedRem := originalRem.Merge(partial)

	statement, _ := r.repo.Prepare("UPDATE reminders SET title = ?, description = ?, date = ?, notified = ?, should_alert = ? WHERE id = ?")
	_, err := statement.Exec(updatedRem.title, updatedRem.description, updatedRem.date, updatedRem.notified, updatedRem.shouldAlert, originalRem.id)
	if err != nil {
		return originalRem, err
	}

	return updatedRem, nil
}

func (r ReminderRepo) list(filter_by FilterBy) ([]Reminder, error) {

	var rows *sql.Rows
	var err error

	switch filter_by {
	case Pending:
		rows, err = r.repo.Query("SELECT id, title, description, date, notified, should_alert FROM reminders WHERE notified = 0")
	case Done:
		rows, err = r.repo.Query("SELECT id, title, description, date, notified, should_alert FROM reminders WHERE notified = 1")
	default:
		rows, err = r.repo.Query("SELECT id, title, description, date, notified, should_alert FROM reminders")
	}

	if err != nil {
		return make([]Reminder, 0), err
	}

	return parseRowsToReminders(rows)
}

func parseRowsToReminders(rows *sql.Rows) ([]Reminder, error) {
	res := make([]Reminder, 0)
	var id int64
	var title string
	var description string
	var date string
	var notified bool
	var should_alert bool

	for rows.Next() {
		rows.Scan(&id, &title, &description, &date, &notified, &should_alert)
		real_date, date_err := time.Parse(time.RFC3339, date)
		if date_err != nil {
			return res, date_err
		}
		rem := Reminder{
			id:          id,
			title:       title,
			date:        real_date,
			description: description,
			notified:    notified,
			shouldAlert: should_alert,
		}
		res = append(res, rem)
	}
	return res, nil
}
