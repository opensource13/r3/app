module gitlab.com/l143/golang/r3mindr

go 1.14

require (
	github.com/gen2brain/beeep v0.0.0-20210529141713-5586760f0cc1
	github.com/gopherjs/gopherjs v0.0.0-20210803090616-8f023c250c89 // indirect
	github.com/mattn/go-sqlite3 v1.14.8
	github.com/robfig/cron/v3 v3.0.1
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.2.1
	golang.org/x/sys v0.0.0-20210818153620-00dd8d7831e7 // indirect
)
