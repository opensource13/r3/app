package cli

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/l143/golang/r3mindr/reminder"
)

const show_flag = "show"

func CommandList() *cobra.Command {
	listCmd := &cobra.Command{
		Use:   "list",
		Short: "List all reminders",
		Run:   runListFn(),
	}

	listCmd.Flags().StringP(show_flag, "s", "All", "Specify which shouls be returned ")

	return listCmd
}

func runListFn() CobraFn {
	return func(cmd *cobra.Command, args []string) {
		show, _ := cmd.Flags().GetString(show_flag)

		var showEnum reminder.FilterBy
		switch show {
		case "Pending":
			showEnum = reminder.Pending
		case "Done":
			showEnum = reminder.Done
		default:
			showEnum = reminder.All
		}

		reminders, err := reminder.GetReminders(showEnum)

		if err != nil {
			panic(err)
		}
		fmt.Println("Listing reminders")
		for _, val := range reminders {
			fmt.Println(val)
		}
	}
}
