package reminderScheduler

import (
	"fmt"
	"time"

	"gitlab.com/l143/golang/r3mindr/reminder"
	"gitlab.com/l143/golang/r3mindr/storage"

	"github.com/gen2brain/beeep"
	"github.com/robfig/cron/v3"
	log "github.com/sirupsen/logrus"
)

type Scheduler struct {
	cron      *cron.Cron
	relations map[int64]cron.EntryID
}

func NewReminderScheduler(c *cron.Cron) *Scheduler {
	return &Scheduler{
		cron:      c,
		relations: make(map[int64]cron.EntryID),
	}
}

func (s Scheduler) SetRedminders(reminders []reminder.Reminder) (uint8, error) {

	var realPending uint8 = 0
	for _, rem := range reminders {

		fnTmp := runFn(rem, s)
		now := time.Now()
		if now.After(rem.GetDate()) {
			fnTmp()
		} else {
			realPending += 1
			notation := rem.GetCronNotation()
			id, cronErr := s.cron.AddFunc(notation, fnTmp)
			if cronErr != nil {
				log.WithFields(log.Fields{"Reminder": rem.GetLogString(), "error": cronErr}).Error("Cron operation")
			}
			s.relations[rem.GetId()] = id
		}
	}

	return realPending, nil
}

func runFn(rem reminder.Reminder, scheduler Scheduler) func() {
	notification := rem.GetNotify()

	return func() {
		imageNotification := fmt.Sprintf("%s/assets/icon1x.png", storage.GetAppPath())
		err := beeep.Notify(notification.Title, notification.Text, imageNotification)
		if err != nil {
			msg := fmt.Sprintf("Notification problem for %s = %s", rem, err)
			log.Error(msg)
		} else {
			partial := reminder.UpdateReminderDto{}
			notified := true
			partial.Notified = &notified

			updated, updateErr := reminder.UpdateReminder(rem, partial)
			if updateErr != nil {
				log.WithFields(log.Fields{"Reminder": rem.GetLogString(), "error": updateErr}).Error("Notified but not updated in db")
			} else {
				log.WithFields(log.Fields{"Reminder": updated.GetLogString()}).Info("Notified")
				scheduler.cron.Remove(scheduler.relations[rem.GetId()])
			}
		}
	}
}
