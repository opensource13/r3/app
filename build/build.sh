#!/bin/bash

SHOULD_ZIP=0
BUILD_DIR="./dist"
BUILD_FILE="r3.zip"

function build {
    echo "[1] Building ..."
    # Build the client
    go build cmd/cli/r3-cli.go

    # Build the server
    go build cmd/server/r3-server.go
}

function internal_zip {
    echo "[2] Zipping ..."

    if [ ! -d "${BUILD_DIR}" ]; then
        mkdir -p "${BUILD_DIR}"
    fi

    zip "$BUILD_DIR/$BUILD_FILE" r3-cli
    zip "$BUILD_DIR/$BUILD_FILE" r3-server
    zip "$BUILD_DIR/$BUILD_FILE" assets/icon1x.png
}

function clean {
    echo "[3] Cleaning ..."
    rm r3-cli
    rm r3-server
}

function help {
    echo -e " ####### HELP


-z Will the content into r3.zip

"
}

while getopts zh options; do
    case "$options" in
	    z)	
            SHOULD_ZIP=1
            ;;
        h) 
            help;
            exit 0
            ;;
	    [?])
		    exit 1;;
	esac
done

build;
if [ $SHOULD_ZIP == 1 ]; then
    internal_zip;
    clean;
fi