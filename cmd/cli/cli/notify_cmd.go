package cli

import (
	"fmt"

	"github.com/gen2brain/beeep"
	"github.com/spf13/cobra"
	"gitlab.com/l143/golang/r3mindr/reminder"
)

const id_flag = "id"

func CommandNotify() *cobra.Command {
	notifyCmd := &cobra.Command{
		Use:   "notify",
		Short: "notify",
		Run:   runNotifyCmd(),
	}

	notifyCmd.Flags().Int64P(id_flag, "i", 0, "Id of the reminder to notificate")
	return notifyCmd
}

func runNotifyCmd() CobraFn {
	return func(cmd *cobra.Command, args []string) {
		id, _ := cmd.Flags().GetInt64(id_flag)
		switch {
		case id == 0:
			fmt.Println("Id incorrecto")
		default:
			rem, err := reminder.GetReminder(id)

			if err != nil {
				fmt.Println(err)
			} else {
				notification := rem.GetNotify()
				err := beeep.Notify(notification.Title, notification.Text, "./assets/icon1x.png")
				if err != nil {
					panic(err)
				}
			}

		}
	}
}
