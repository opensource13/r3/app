#!/bin/bash

R3_DIR=".r3"
INSTALL_DIR="${HOME}/${R3_DIR}"
LOCAL_PULL=""
REMOTE_PULL="https://gitlab.com/l143/golang/r3mindr/-/wikis/uploads/b817774da15ed57ef331e36670dd1a95/r3.zip"

function help {
    echo -e " ####### HELP


-l Specify a local pull path

"
}

function creatr_dir {
    if [ ! -d "${INSTALL_DIR}" ]; then
        echo "[+] Creating .r3 dir ..."
        mkdir -p "$INSTALL_DIR";
    fi
}

function pull {
    if [ -z "$LOCAL_PULL" ]; then
        echo "[+] Fetching from url  to $INSTALL_DIR/r3.zip ..."
        wget "$REMOTE_PULL";
        mv "r3.zip" "$INSTALL_DIR";
    else
        echo "[+] Copyng from local ..."
        cp "$LOCAL_PULL" "$INSTALL_DIR";
    fi

    cd "$INSTALL_DIR" || exit;
    exists=$(ls | grep -c ".zip");
    if [ "$exists" == "0" ]; then 
        echo "No .zip file to install"
        exit 1;
    fi
}

function first_clean {
    cd "${INSTALL_DIR}" || exit 1;
    
    back=$(ls -I '*.back' -I '*.db' -I '*.zip');
    if [ -n "$back" ]; then 
        echo "[+] Cleaning ..."
        
        backdir="$(date).back"
        mkdir "$backdir"

        for i in $back; do
            mv "$i" "$backdir"
        done
    fi
}

function internal_unzip {
    # should execute inside INSTALL_DIR
    cd "${INSTALL_DIR}" || exit 1;
    echo "[+] Inflating ..."
    unzip r3.zip && rm r3.zip;
    ./r3-cli init;
}



while getopts l:h options; do
    case "$options" in
	    l)	
            LOCAL_PULL="$OPTARG"
            ;;
        h) 
            help;
            exit 0
            ;;
	    [?])
		    exit 1;;
	esac
done


creatr_dir && pull && first_clean && internal_unzip;
