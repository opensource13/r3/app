package cli

import (
	"bytes"
	"fmt"
	"net"

	"github.com/spf13/cobra"
	"gitlab.com/l143/golang/r3mindr/reminder"
)

type CobraFn func(cmd *cobra.Command, args []string)

const url string = "localhost:7233" // url of server to notify

const date_flag = "date"
const title_flag = "title"
const description_flag = "description"
const silence_flag = "silence"
const force_flag = "force"

func CommandAdd() *cobra.Command {
	addCmd := &cobra.Command{
		Use:   "add",
		Short: "Add a reminder",
		Run:   runAddFn(),
	}

	addCmd.Flags().StringP(date_flag, "d", "", "Date for the Reminder in RFC822 Format")
	addCmd.Flags().StringP(title_flag, "t", "", "Description for the reminder")
	addCmd.Flags().StringP(description_flag, "D", "", "Description for the reminder")
	addCmd.Flags().BoolP(silence_flag, "s", false, "Silent reminder")
	addCmd.Flags().BoolP(force_flag, "f", false, "Force to create")
	return addCmd
}

func runAddFn() CobraFn {
	return func(cmd *cobra.Command, args []string) {
		date, _ := cmd.Flags().GetString(date_flag)
		title, _ := cmd.Flags().GetString(title_flag)
		description, _ := cmd.Flags().GetString(description_flag) // Empty string is ok
		silent, _ := cmd.Flags().GetBool(silence_flag)
		force, _ := cmd.Flags().GetBool(force_flag)

		switch {
		case title == "":
			missingRequiredFlag("Title not specified")
		case date == "":
			missingRequiredFlag("Date not specified")
		default:
			rem, err := reminder.CreateReminder(title, description, date, silent, force)
			if err == nil {
				fmt.Printf("[Added] %s\n", rem)
				conn, err := net.Dial("tcp", "localhost:7233")
				if err != nil {
					fmt.Println("Error in notifyin server")
				}
				msg := fmt.Sprintf("%d\n", rem.GetId())
				message := bytes.NewBufferString(msg)
				conn.Write(message.Bytes())
				conn.Close()
			} else {
				errorMsg := fmt.Sprintf("Could no create reminder because %s", err)
				fmt.Println(errorMsg)
			}
		}
	}
}

func missingRequiredFlag(message string) {
	fmt.Println(message)
}
