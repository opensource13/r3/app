package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/l143/golang/r3mindr/cmd/cli/cli"
)

func main() {
	rootCmd := &cobra.Command{Use: "r3"}
	rootCmd.AddCommand(cli.CommandAdd())
	rootCmd.AddCommand(cli.CommandList())
	rootCmd.AddCommand(cli.CommandExport())
	rootCmd.AddCommand(cli.CommandInit())
	rootCmd.AddCommand(cli.CommandNotify())
	rootCmd.Execute()
}
