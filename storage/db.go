package storage

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

const db_name = "r3.db"
const app_path = ".r3"

func GetAppPath() string {
	return fmt.Sprintf("%s/%s", os.Getenv("HOME"), app_path)
}

func GetDBPath() string {
	return fmt.Sprintf("%s/%s", GetAppPath(), db_name)
}

func InitDB() {
	if _, err := os.Stat(GetAppPath()); os.IsNotExist(err) {
		os.MkdirAll(GetAppPath(), os.ModePerm)
	}
	database, _ := sql.Open("sqlite3", GetDBPath())
	statement, _ := database.Prepare(`
		CREATE TABLE IF NOT EXISTS reminders (
			id INTEGER PRIMARY KEY,
			title TEXT NOT NULL,
			description TEXT NOT NULL,
			date timestamp NOT NULL,
			notified NUMERIC DEFAULT 0,
			should_alert NUMERIC DEFAULT 1
		)
	`)
	statement.Exec()
}
