package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strconv"

	"github.com/robfig/cron/v3"
	log "github.com/sirupsen/logrus"
	scheduler "gitlab.com/l143/golang/r3mindr/cmd/server/scheduler"
	"gitlab.com/l143/golang/r3mindr/reminder"
	"gitlab.com/l143/golang/r3mindr/storage"
)

func init() {
	log.SetFormatter(&log.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})
	log.SetLevel(log.InfoLevel)
}

func main() {
	storage.InitDB()
	log.Info("R3 Init Database", storage.GetDBPath())

	log.Info("R3 Reminder Server")
	c := cron.New()

	log.Info("Reading db")
	reminders, err := reminder.GetReminders(reminder.Pending)
	checkError(err)

	rS := scheduler.NewReminderScheduler(c)
	nReminders, _ := rS.SetRedminders(reminders)

	log.WithFields(log.Fields{"Number of Reminders": nReminders}).Info("Crons Setted ")
	log.Info("😈 R3 Start")
	c.Start()

	service := ":7233"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", service)
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)
	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}

		go handleServerEnrty(conn, rS)
	}

}

func handleServerEnrty(conn net.Conn, scheduler *scheduler.Scheduler) {
	buffer, err := bufio.NewReader(conn).ReadBytes('\n')

	if err != nil {
		fmt.Println("Client left.")
		conn.Close()
		return
	}

	var id int64
	rawMsg := (string(buffer[:len(buffer)-1]))
	id, convertErr := strconv.ParseInt(rawMsg, 10, 64)

	if convertErr != nil {
		log.Error("Received id reminder but could not add to cron, Convert err")
	} else {
		rem, remErr := reminder.GetReminder(id)
		if remErr != nil {
			remErrMsg := fmt.Sprintf("Received %d id reminder but could not add to cron: %s", id, remErr)
			log.Error(remErrMsg)
		} else {
			tmpSplice := [1]reminder.Reminder{rem}
			nReminders, cronErr := scheduler.SetRedminders(tmpSplice[:])
			if cronErr != nil {
				log.Error(cronErr)
			} else {
				log.WithFields(log.Fields{"Number of Reminders": nReminders}).Info("Crons Setted by socket")
			}
		}
	}
}

func checkError(err error) {
	if err != nil {
		log.Error(err)
		os.Exit(1)
	}
}
