package reminder

import (
	"fmt"
	"time"
)

type Reminder struct {
	id          int64
	title       string
	description string
	date        time.Time
	notified    bool
	shouldAlert bool
}

type ReminderNotification struct {
	Title string
	Text  string
}

func (rem Reminder) GetNotify() ReminderNotification {

	if rem.description != "" {
		return ReminderNotification{
			Title: rem.title,
			Text:  rem.description,
		}
	}

	return ReminderNotification{
		Title: "R3minder",
		Text:  rem.title,
	}
}

func (rem Reminder) String() string {
	return fmt.Sprintf("Reminder %s on %s (%d)", rem.title, rem.date.Format("Mon 02-Jan-2006 at 15:04"), rem.id)
}

func (rem Reminder) GetLogString() string {
	return fmt.Sprintf("[%d] '%s' at %s", rem.id, rem.title, rem.date.Format("Mon 02-Jan-2006 at 15:04"))
}

func (rem Reminder) GetCronNotation() string {
	return rem.date.Format("04 15 02 Jan Mon")
}

func (rem Reminder) GetDate() time.Time {
	return rem.date
}

func (rem Reminder) GetId() int64 {
	return rem.id
}

func (rem Reminder) Merge(partial UpdateReminderDto) Reminder {
	newTitle := rem.title
	newDescription := rem.description
	newDate := rem.date
	newNotified := rem.notified
	newShouldAlert := rem.shouldAlert

	if partial.Title != nil {
		newTitle = *partial.Title
	}
	if partial.Description != nil {
		newDescription = *partial.Description
	}
	if partial.Date != nil {
		newDate = *partial.Date
	}
	if partial.Notified != nil {
		newNotified = *partial.Notified
	}
	if partial.ShouldAlert != nil {
		newShouldAlert = *partial.ShouldAlert
	}

	return Reminder{
		id:          rem.id,
		title:       newTitle,
		description: newDescription,
		date:        newDate,
		notified:    newNotified,
		shouldAlert: newShouldAlert,
	}
}

func EmptyReminder() Reminder {
	return Reminder{
		id:          0,
		date:        time.Now(),
		description: "Invalid Reminder",
		notified:    false,
		shouldAlert: false,
	}
}
