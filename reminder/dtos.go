package reminder

import "time"

type CreateReminderDto struct {
	title       string
	description string
	date        time.Time
	shouldAlert bool
}

type UpdateReminderDto struct {
	Title       *string
	Description *string
	Date        *time.Time
	ShouldAlert *bool
	Notified    *bool
}

type FilterBy string

const (
	All     FilterBy = "All"
	Pending          = "Pending"
	Done             = "Done"
)
